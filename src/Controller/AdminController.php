<?php

namespace App\Controller;

use App\Entity\Prospect;
use App\Service\CsvExporter;
use App\Service\Encrypter;
use App\Service\MailSender;
use App\Service\PdfMaker;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Error\Error;

class AdminController extends BaseAdminController
{


    /**
     * @Route("/prospect/resendMail", name="resend_mail")
     * @param Request $request
     * @param MailSender $mailer
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @internal param PdfMaker $pdf
     */
    public function resendProspectAction(Request $request,  MailSender $mailer, EntityManagerInterface $em)
    {
        $id = $request->get('id');

        /** @var Prospect $prospect */
        $prospect =  $em->getRepository('App:Prospect')->find($id);


        $status = $mailer->sendMail($prospect);

        $msg = $status === true ? 'Mail succesfully sent' : $status->getMessage();


        $request->getSession()
            ->getFlashBag()
            ->add('success',$msg);
        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * @Route("/dashboard", name="admin_dashboard")
     */
    public function dashboardAction(EntityManagerInterface $em)
    {
        $news = $em->getRepository('App:Config')->findOneBy(['name' => 'news']);
        return $this->render(
            'Admin/dashboard.html.twig',array(
                'news' => $news
            )
        );

    }


    /**
     *
     * @Route("/enable_news", name="enable_news")
     */
    public function enableNewsAction(Request $request, EntityManagerInterface $em)
    {
        $news = $em->getRepository('App:Config')->findOneBy(['name'=>'news']);
        $em->persist($news);


        $news->setValue($request->get('news') == 'true' ? 1:0);
        $em->flush();
        return new JsonResponse('ok');


    }



    public function exportAction()
    {
        echo "where is this";
        throw new \RuntimeException('Action for exporting an entity not defined');
    }
}