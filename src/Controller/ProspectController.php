<?php
/**
 * Created by PhpStorm.
 * User: tudor
 * Date: 24.08.2017
 * Time: 15:05
 */

namespace App\Controller;

use App\Entity\Prospect;
use App\Service\CsvExporter;


class ProspectController extends AdminController
{

    private $csvExporter;
    public function __construct(CsvExporter $csvExporter)
    {
        $this->csvExporter = $csvExporter;
    }

    public function exportAction()
    {
        //TODO: export action code right here;
        //TODO: Since the solution is 'hacky-ish', I must check if user is logged in ; But maybe this is already checked with /admin Rules

        $sortDirection = $this->request->query->get('sortDirection');
        if (empty($sortDirection) || !in_array(strtoupper($sortDirection), ['ASC', 'DESC'])) {
            $sortDirection = 'DESC';
        }

        $queryBuilder = $this->createListQueryBuilder(
            Prospect::class,
            $sortDirection,
            $this->request->query->get('sortField'),
            $this->entity['list']['dql_filter']
        );

        return $this->csvExporter->getResponseFromQueryBuilder(
            $queryBuilder,
            Prospect::class,
            'prospects.csv'
        );
    }

}