<?php
namespace App\Controller;

use App\Entity\Prospect;
use App\Service\Encrypter;
use App\Service\MailSender;
use App\Service\PdfMaker;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Ivory\CKEditorBundle\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Twig\Error\Error;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

class MainController extends Controller
{


    public function preIndexAction()
    {


        $aLangs = ['nl','fr'];
        $langs = $this->prefered_language($aLangs, $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
        $extra = key($langs) ? key($langs): 'nl';
        return $this->redirectToRoute('index',array(
            '_locale' => $extra
        ));
    }



    private function prefered_language(array $available_languages, $http_accept_language) {

        $available_languages = array_flip($available_languages);

        $langs = [];
        preg_match_all('~([\w-]+)(?:[^,\d]+([\d.]+))?~', strtolower($http_accept_language), $matches, PREG_SET_ORDER);
        foreach($matches as $match) {

            list($a, $b) = explode('-', $match[1]) + array('', '');
            $value = isset($match[2]) ? (float) $match[2] : 1.0;

            if(isset($available_languages[$match[1]])) {
                $langs[$match[1]] = $value;
                continue;
            }

            if(isset($available_languages[$a])) {
                $langs[$a] = $value - 0.1;
            }

        }
        arsort($langs);

        return $langs;
    }


    /**
     *
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction(Request $request, EntityManagerInterface $em)
    {

        return $this->render('index.html.twig', array(

            'news' => $em->getRepository('App:Config')->findOneBy(['name'=> 'news'])->getValue()
        ));
    }

    /**
     * @Route("/{locale}/step1", name="step1")
     */
    public function firstAction(SessionInterface $session, Request $request, EntityManagerInterface $em)
    {
        $id = $session->get('proId', '0');
        $prospect = $id ? $this->getDoctrine()->getRepository('App:Prospect')->find($id) : new Prospect();
        $form = $this->createFormBuilder($prospect)
            ->add('name',null,array(
                'label' => 'name',
                'attr'=>['placeholder' => 'name','required' => 'required']
            ))
            ->add('kbo',null,array('label'=>'kbo', 'attr'=>['placeholder'=> 'kbo','required' => 'required', 'onkeypress'  => 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8 || event.charCode == 46 || event.charCode == 9']))
            ->add('postCode',TextType::class,array('label'=>'postcode', 'required' => false, 'attr'=>['placeholder'=> 'postcode']))
            ->add('street',TextType::class,array('label'=>'street', 'required' => false, 'attr'=>['placeholder'=> 'street']))
            ->add('contact',null,array('label'=>'contact', 'attr'=>['placeholder' => 'contact','required' => 'required']))
            ->add('telephone',null,array('label'=>'telephone', 'attr'=>['placeholder'=> 'telephone', 'onkeypress'  => 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8 || event.charCode == 46 || event.charCode == 9','required' => 'required']))
            ->add('email',null,array('label'=>'email', 'attr'=>['placeholder'=> 'email','required' => 'required']))
            ->add('sector', ChoiceType::class, array(
                'choices' => array(
                    'choice.select' => '',
                    'choice.sector.1' => 'Diensten (ICT & telecom, utilities)',
                    'choice.sector.2' => 'Horeca',
                    'choice.sector.3' => 'Industrie (metaal, hout, textiel)',
                    'choice.sector.4' => 'Transport / logistiek',
                    'choice.sector.5' => 'Bouw / afwerking bouw',
                    'choice.sector.6' => 'Groot- en kleinhandel',
                    'choice.sector.7' => 'Social profit / publieke sector',
                    'choice.sector.8' => 'Chemie en biotechnologie',
                    'choice.sector.9' => 'Voedingsnijverheid',
                    'choice.sector.10' => 'Andere'
                ),
                'required' => false,
                'label'=>'sector'
            ))

            ->add('tractor',null,array('label'=>'tractor', 'attr'=>['placeholder'=> 'tractor', 'onkeypress'  => 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8 || event.charCode == 46 || event.charCode == 9','required' => 'required']))
            ->add('trailers',null,array('label'=>'trailers', 'attr'=>['placeholder'=> 'trailers', 'onkeypress'  => 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8 || event.charCode == 46 || event.charCode == 9','required' => 'required']))
            ->add('hasTruck', ChoiceType::class, array(
                'choices' => array(
                    'yes' => true,
                    'no' => false
                ),
                'label'=>'hasTruck',
                'attr' => array(
                    'required'=> 'required'
                ),
                'placeholder' => 'choice.select'
            ))
            ->add('activity',null,array('attr'=> array('required' => 'required')))
            ->add('otherActivity',null,array(
                    'label' => 'other'
            ))
            ->add('goods',null,array('attr'=> array('required' => 'required')))

            ->add('isRisky', ChoiceType::class, array(
                'choices' => array(
                    'yes' => true,
                    'no' => false
                ),
                'label'=>'isRisky',
                'placeholder' =>  'choice.select'
            ))
            ->add('otherRisks',null,array(
                'label' => 'other'
            ))
            ->add('hasRegulation', ChoiceType::class, array(
                'choices' => array(
                    'yes' => true,
                    'no' => false
                ),
                'label'=>'hasRegulation',
                'attr' => array(
                    'required'=> 'required'
                ),
                'placeholder' => 'choice.select'
            ))
            ->add('regulations',null,array('label'=>'regulations',  'required' => false, 'attr'=>['placeholder'=> 'regulations']))

            ->add('region',null,array('attr'=> array('required' => 'required')))
            ->add('extraRegion',null,array('label'=>'extraRegion', 'required' => false, 'attr'=>['placeholder'=> 'extraRegion']))
            ->add('hasBranch', ChoiceType::class, array(
                'choices' => array(
                    'yes' => 1,
                    'no' => 0
                ),
                'placeholder' => 'choice.select',
                'label'=>'hasBranch',
                'attr' => array(
                    'required'=> 'required'
                )
            ))
            ->add('extraBranch',null,array('label'=>'extraBranch', 'required' => false, 'attr'=>['placeholder'=> 'extraBranch']))

            ->add('isContracter', ChoiceType::class, array(
                'choices' => array(
                    'yes' => 1,
                    'no' => 0
                ),
                'label'=>'isContracter',
                'attr' => array(
                    'required'=> 'required'
                ),
                'placeholder' => 'choice.select'
            ))

            ->add('contracterCountries',null,array('label'=>'contracterCountries', 'required' => false, 'attr'=>['placeholder'=> 'contracterCountries']))
            ->add('hasAdditional', ChoiceType::class, array(
                'choices' => array(
                    'yes' => 1,
                    'no' => 0
                ),
                'label'=>'hasAdditional',
                'attr' => array(
                    'required'=> 'required'
                ),
                'placeholder' => 'choice.select'

            ))
//            ->add('additionalDetails',null,array('label'=>'additionalDetails', 'attr'=>['placeholder'=> 'additionalDetails']))
            ->add('turnover',null,array('label'=>'turnover', 'required' => false, 'attr'=>['placeholder'=> 'turnover', 'onkeypress'  => 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8 || event.charCode == 46 || event.charCode == 9']))
            ->add('employees',null,array('label'=>'employees', 'required' => false, 'attr'=>['placeholder'=> 'employees', 'onkeypress'  => 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8 || event.charCode == 46 || event.charCode == 9']))
            ->add('save', SubmitType::class)
            ->getForm();


        $em->persist($prospect);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();
            $session->set('proId', $prospect->getId());

            return $this->redirectToRoute('step2');
        }


        return $this->render('step-1.html.twig', array(
            'form' => $form->createView(),
            'news' => $em->getRepository('App:Config')->findOneBy(['name'=> 'news'])->getValue()

    ));
    }


    /**
     *
     * @Route("/{_locale}/step2", name="step2")
     *
     * @return Response
     */
    public function secondAction(EntityManagerInterface $em, SessionInterface $session)
    {

        $products = $em->getRepository('App:Product')->findAll();


        $id = $session->get('proId', '0');
        if (!$id){
            return $this->redirectToRoute('step1');
        }
        $prospect = $id ? $this->getDoctrine()->getRepository('App:Prospect')->find($id) : new Prospect();

        $constraints = [];
        foreach($products as $product){
            if(strtolower($product->getName()) === 'vrd' ){
                $constraints['vrd'][3] =  $prospect->getHasTruck() ? 'show-opt':'hide-opt';
            }
            if(strtolower($product->getName()) === 'cmr'){
                $constraints['cmr'][1] = strpos(strtolower($prospect->getGoods()), 'koeltransport') !== false ? 'show-opt':'hide-opt';
                $constraints['cmr'][2] = $prospect->getisRisky()  ? 'show-opt':'hide-opt';
                $constraints['cmr'][3] = strpos(strtolower($prospect->getActivity()),'cta') !== false ? 'show-opt':'hide-opt';
                $constraints['cmr'][4] = $prospect->getisContracter() ? 'show-opt':'hide-opt';
                $constraints['cmr'][5] = $prospect->getHasAdditional() ? 'show-opt':'hide-opt';
                $constraints['cmr'][6] = $constraints['cmr'][4];
                $constraints['cmr'][0] = 'hide-opt';
                for ($i = 1 ; $i <= 6; $i++){
                    if($constraints['cmr'][0] == 'show-opt'){
                        $constraints['cmr'][0] = 'show-opt';
                    }
                }
            }
        }

        return $this->render('step-2.html.twig',array(
            'products'  =>  $products,
            'prospect'  => $prospect,
            'constraints' => $constraints,
            'news' => $em->getRepository('App:Config')->findOneBy(['name'=> 'news'])->getValue()
        ));
    }

    /**
     *
     * @Route("/step3", name="step3")
     *
     * @return Response
     */
    public function thirdAction(SessionInterface $session, Request $request, EntityManagerInterface $em)
    {
        $products = ['BA', 'CMR', 'VRD'];

        $id = $session->get('proId', '0');
        if ($id == '0') {
            return $this->redirectToRoute('step1');
        }


        $prospect = $em->getRepository('App:Prospect')->find($id);
        $em->persist($prospect);

        $optionals = [];
        $extras = [];
        $optionals['BA'] = '';
        $optionals['CMR'] = '';
        $optionals['VRD'] = '';
        $extras['BA'] = '';
        $extras['CMR'] = '';
        $extras['VRD'] = '';
        $prodString = '';

        foreach ($products as $product) {
            if ($request->get($product)) {
                $prodString .= $product . ';';
            }
            for($i=1;$i<=6;$i++){
                if ($request->get($product.'_opt'.$i)) {
                    $optionals[$product] .= $i . ';';
                }
                if ($request->get($product.'_ext'.$i)) {
                    $extras[$product] .= $i . ';';
                }
            }
        }

        $prospect->setProducts($prodString);
        $prospect->setOptionalsBa($optionals['BA']);
        $prospect->setOptionalsVrd($optionals['VRD']);
        $prospect->setOptionalsCmr($optionals['CMR']);
        $prospect->setExtrasVrd($extras['VRD']);
        $em->flush();

        return $this->render('step-3.html.twig', array(
            'news' => $em->getRepository('App:Config')->findOneBy(['name'=> 'news'])->getValue()

        ));
    }

    /**
     *
     * @Route("/thank-you", name="thank-you")
     *
     * @param SessionInterface $session
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param MailSender $mail
     * @return Response
     */
    public function thankYouAction(SessionInterface $session, Request $request, EntityManagerInterface $em, MailSender $mail)
    {

        $id = $session->get('proId', '0');
        if ($id == '0') {
            return $this->redirectToRoute('step1');
        }

        /** @var Prospect $prospect */
        $prospect = $em->getRepository('App:Prospect')->find($id);
        $em->persist($prospect);

        $emailString = $request->get('emails');

        $prospect->setSenderEmail($emailString);
        $em->flush();


        $mail->sendMail($prospect);

        $session->set('proId', '0');

        return $this->render('thank-you.html.twig', array(
            'news' => $em->getRepository('App:Config')->findOneBy(['name'=> 'news'])->getValue()
        ));
    }



    public function testmailtemAction(Encrypter $encrypter)
    {

        $id = 1;
        $hash = $encrypter->encrypt($id);

        return $this->render('Mail/template.html.twig',array(
            'id' => $id,
            'hash' => $hash,
            'locale' => 'nl'
        ));

    }


    public function testpdftemAction()
    {
        $em = $this->getDoctrine();
        $prospect = $em->getRepository('App:Prospect')->find(9);
        $ba =  $em->getRepository('App:Product')->findOneBy(['name' => 'BA']);
        $vrd = $em->getRepository('App:Product')->findOneBy(['name' => 'VRD']);
        $cmr = $em->getRepository('App:Product')->findOneBy(['name' => 'CMR']);

        
        return $this->render('pdf.html.twig',
            array(
                'prospect' => $prospect,
                'ba' => $ba,
                'vrd' => $vrd,
                'cmr' => $cmr,
                'locale' => $prospect->getLang()
            ));

    }


    /**
     *
     * @Route("/news", name="news")
     *
     */
    public function newsAction(EntityManagerInterface $em)
    {

        $news = $this->getDoctrine()->getRepository('App:News')->findAll();
        $template = $news ? 'nieuws' : 'index' ;
        return $this->render($template . '.html.twig',array(
            'newses' => $news,
            'news' => $em->getRepository('App:Config')->findOneBy(['name'=> 'news'])->getValue()

        ));
    }


    public function testmailAction(MailSender $mailer)
    {
        $em = $this->getDoctrine();

        $prospect = $em->getRepository('App:Prospect')->find(9);


        $prospect->setSenderEmail('tudor.placinta@expertnetwork.ro');
        $mailer->sendMail($prospect);


        return $this->render('thank-you.html.twig');


    }



    public function pdfAction(Request $request, PdfMaker $pdf, Encrypter $encrypter)
    {
        $id = $request->get('id');
        if(!strpos($request->headers->get('referer'),'/admin/')){
            $hash = $request->get('hash');
            $check = $encrypter->encrypt($id);
            if($hash != $check){
                throw new \Exception('Security error!');
            }
        }
        $prospect =  $this->getDoctrine()->getRepository('App:Prospect')->find($id);

        $html = $pdf->createPdf($prospect, true);

        return new PdfResponse(
            $html,'prospect.pdf'
        );

    }

    public function testpdfAction(Request $request, PdfMaker $pdf, Encrypter $encrypter)
    {
//        $id = $request->get('id');
//        if(!strpos($request->heatders->get('referer'),'/admin/')){
//            $hash = $request->get('hash');
//            $check = $encrypter->encrypt($id);
//            if($hash != $check){
//                throw new \Exception('Security error!');
//            }
//        }
        $prospect =  $this->getDoctrine()->getRepository('App:Prospect')->find(7);

        $html = $pdf->createPdf($prospect, true);

        return new PdfResponse(
            $html,'prospect.pdf'
        );

    }

}