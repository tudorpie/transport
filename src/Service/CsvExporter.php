<?php

namespace App\Service;

use App\Entity\Prospect;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvExporter
{
    public function getResponseFromQueryBuilder(QueryBuilder $queryBuilder, $columns, $filename)
    {
        $entities = new ArrayCollection($queryBuilder->getQuery()->getResult());
        $response = new StreamedResponse();
        if (is_string($columns)) {
            $columns = $this->getColumnsForEntity($columns);
        }
        $response->setCallback(function () use ($entities, $columns) {
            $handle = fopen('php://output', 'w+');
            // Add header
            fputcsv($handle, array_keys($columns),',');
            while ($entity = $entities->current()) {
                $values = [];
                foreach ($columns as $column => $callback) {
                    $value = $callback;
                    if (is_callable($callback)) {
                        $value = $callback($entity);
                    }
                    $values[] = $value;
                }
                fputcsv($handle, $values,',');
                $entities->next();
            }
            fclose($handle);
        });
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');
        return $response;
    }


    public function getColumnsForEntity($class)
    {
        $columns[Prospect::class] = [
            'Prospect ID' => function (Prospect $prospect) {
                return $prospect->getId();
            },
            'Email' => function (Prospect $prospect) {
                return $prospect->getEmail();
            },
            'Name' => function (Prospect $prospect) {
                return $prospect->getName();
            },
            'Contact' => function (Prospect $prospect) {
                return $prospect->getContact();
            },
            'Street' => function (Prospect $prospect) {
                return $prospect->getStreet();
            },
            'PostCode' => function (Prospect $prospect) {
                return $prospect->getPostCode();
            },
            'Kbo' => function (Prospect $prospect) {
                return $prospect->getKbo();
            },
            'Telephone' => function (Prospect $prospect) {
                return $prospect->getTelephone();
            },
            'Sector' => function (Prospect $prospect) {
                return $prospect->getSector();
            },
            'Extra' => function (Prospect $prospect) {
                return $prospect->getExtra();
            },
            'Products' => function (Prospect $prospect) {
                return $prospect->getProducts();
            },
            'OptionalsBa' => function (Prospect $prospect) {
                return $prospect->getOptionalsBa();
            },
            'OptionalsCmr' => function (Prospect $prospect) {
                return $prospect->getOptionalsCmr();
            },
            'OptionalsVrd' => function (Prospect $prospect) {
                return $prospect->getOptionalsVrd();
            },
            'ExtrasVrd' => function (Prospect $prospect) {
                return $prospect->getExtrasVrd();
            },
            'Tractor' => function (Prospect $prospect) {
                return $prospect->getTractor();
            },
            'Trailers' => function (Prospect $prospect) {
                return $prospect->getTrailers();
            },
            'HasTruck' => function (Prospect $prospect) {
                return $prospect->getHasTruck();
            },
            'Activity' => function (Prospect $prospect) {
                return $prospect->getActivity();
            },
            'OtherActivity' => function (Prospect $prospect) {
                return $prospect->getOtherActivity();
            },
            'Goods' => function (Prospect $prospect) {
                return $prospect->getGoods();
            },
            'isRisky' => function (Prospect $prospect) {
                return $prospect->getisRisky();
            },
            'OtherRisks' => function (Prospect $prospect) {
                return $prospect->getOtherRisks();
            },
          
            'HasRegulations' => function (Prospect $prospect) {
                return $prospect->getHasRegulation();
            },
          
            'Regulations' => function (Prospect $prospect) {
                return $prospect->getRegulations();
            },
          
            'Region' => function (Prospect $prospect) {
                return $prospect->getRegion();
            },
          
            'ExtraRegion' => function (Prospect $prospect) {
                return $prospect->getExtraRegion();
            },
          
            'hasBranch' => function (Prospect $prospect) {
                return $prospect->getHasBranch();
            },
          
            'ExtraBranch' => function (Prospect $prospect) {
                return $prospect->getExtraBranch();
            },
          
            'IsContracter' => function (Prospect $prospect) {
                return $prospect->getisContracter();
            },
          
            'ContracterCountries' => function (Prospect $prospect) {
                return $prospect->getContracterCountries();
            },
          
            'HasAdditional' => function (Prospect $prospect) {
                return $prospect->getHasAdditional();
            },
          
            'AdditionalDetails' => function (Prospect $prospect) {
                return $prospect->getAdditionalDetails();
            },
          
            'Turnover' => function (Prospect $prospect) {
                return $prospect->getTurnover();
            },
          
            'Employees' => function (Prospect $prospect) {
                return $prospect->getEmployees();
            },
          
            'SenderEmail' => function (Prospect $prospect) {
                return $prospect->getSenderEmail();
            },
          
            'IsSentEmail' => function (Prospect $prospect) {
                return $prospect->getisSentEmail();
            },
          

        ];
        if (array_key_exists($class, $columns)) {
            return $columns[$class];
        }
        throw new \InvalidArgumentException(sprintf(
            'No columns set for "%s" entity',
            $class
        ));
    }
    
    

}