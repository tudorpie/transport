<?php
/**
 * Created by PhpStorm.
 * User: tudor
 * Date: 12.10.2017
 * Time: 14:58
 */

namespace App\Service;


class Encrypter
{

    private $secret;

    public function __construct($secret)
    {
        $this->secret = $secret;

    }
    public function encrypt($id)
    {
        return md5($id.$this->secret);
    }

    public function decrypt($message, $id)
    {
        return md5($id.$this->secret) == $message ? true : false;

    }
}