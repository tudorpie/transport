<?php
/**
 * Created by PhpStorm.
 * User: tudor
 * Date: 05.10.2017
 * Time: 11:28
 */

namespace App\Service;


use Doctrine\ORM\EntityManager;
use Knp\Snappy\Pdf;
use Twig_Environment;

class PdfMaker
{


    public function __construct(Twig_Environment $twig, EntityManager $doctrine, Pdf $snappy, $dir )
    {
        $this->twig = $twig;
        $this->doctrine = $doctrine;
        $this->snappy = $snappy;
        $this->dir = $dir;
    }



    public function createPdf($prospect)
    {
        $em = $this->doctrine;
        $twig = $this->twig;
        $pdf = $this->snappy;

        $ba =  $em->getRepository('App:Product')->findOneBy(['name' => 'BA']);
        $vrd = $em->getRepository('App:Product')->findOneBy(['name' => 'VRD']);
        $cmr = $em->getRepository('App:Product')->findOneBy(['name' => 'CMR']);

        $html = $twig->render('pdf.html.twig',
            array(
                'prospect' => $prospect,
                'ba' => $ba,
                'vrd' => $vrd,
                'cmr' => $cmr,
                'locale' => $prospect->getLang()
            ));

        $header = $twig->render('html/header.html.twig');
        $footer = $twig->render('html/footer.html.twig');

        $pdf->setOption('header-html',$header);
        $pdf->setOption('footer-html',$footer);
        $pdf->setOption('header-spacing',10);
        $pdf->setOption('footer-spacing',10);

        return $pdf->getOutputFromHtml($html);
    }




    public function testTemPdf($prospect, $getOutput = false)
    {
        $em = $this->doctrine;
        $twig = $this->twig;
        $pdf = $this->snappy;
        $dir = $this->dir;
        $ba =  $em->getRepository('App:Product')->findOneBy(['name' => 'BA']);
        $vrd = $em->getRepository('App:Product')->findOneBy(['name' => 'VRD']);
        $cmr = $em->getRepository('App:Product')->findOneBy(['name' => 'CMR']);

        $html = $twig->render('pdf.html.twig',
            array(
                'prospect' => $prospect,
                'ba' => $ba,
                'vrd' => $vrd,
                'cmr' => $cmr,
                'locale' => 'nl'
            ));

        $header = $twig->render('html/header.html.twig');
        $footer = $twig->render('html/footer.html.twig');
        $pdf->setOption('header-html',$header);
        $pdf->setOption('footer-html',$footer);
        $pdf->setOption('header-spacing',10);
        $pdf->setOption('footer-spacing',10);

        if($getOutput){
            return $pdf->getOutputFromHtml($html);
        }

        $path = $dir .'/public/uploads/pdf/genpdf'.time().'.pdf' ;
        $pdf->generateFromHtml($html,$path);

        return $path;
    }



}