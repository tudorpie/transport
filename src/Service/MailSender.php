<?php

namespace App\Service;

use App\Entity\Prospect;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

class MailSender
{


    private $twig;
    private $mailer;
    private $em;
    private $encrypter;
    private $logger;

    public function __construct(\Twig_Environment $twig, \Swift_Mailer $mailer, EntityManagerInterface $em, Encrypter $encrypter, LoggerInterface $logger)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->em = $em;
        $this->encrypter = $encrypter;
        $this->logger = $logger;
    }

    /**
     * @param Prospect $prospect
     * @param string $locale
     * @return bool|Exception
     * @internal param string $path
     * @internal param string $emailString
     */
    public function sendMail(Prospect $prospect)
    {

        ini_set('memory_limit', '-1');
        $emailString = $prospect->getSenderEmail();
        $mailList = preg_split('@;@', $emailString, NULL, PREG_SPLIT_NO_EMPTY);

        $hash = $this->encrypter->encrypt($prospect->getId());

        try {

            $message = new \Swift_Message();
            $message
                ->setSubject('Safer Transport - Product info')
                ->setFrom(['nocontact@baloise.be' => 'Baloise'])
                ->setTo($mailList)
                ->setBody(
                    $this->twig->render('Mail/template.html.twig',array(
                        'id' => $prospect->getId(),
                        'hash' =>  $hash,
                        'locale' => $prospect->getLang()

                    )),'text/html')

            ;


            $wasSent = $this->mailer->send($message, $errors);

            $this->logger->log('INFO','Message status: '.$wasSent);
            if($errors){
                $this->logger->log('INFO','Errors:'.$errors);
            }

        } catch (Exception $e) {
            $this->logger->log('Info', $e->getMessage());
            return $e;

        }

        $this->em->persist($prospect);
        $prospect->setIsSentEmail(true);
        $this->em->flush();

        return true;


    }

    public function sendtestMail()
    {
    try{
        $message = new \Swift_Message();
        $message
            ->setSubject('Safer Transport - Product info')
            ->setFrom(['nocontact@test.be' => 'Baloise'])
            ->setTo('placinta.tudor@gmail.com')
            ->setBody('test');

        $this->mailer->send($message);

    } catch (Exception $e) {

        dump($e);

        }
        dump($message);
        return 'succcccccceeees';
            }
}