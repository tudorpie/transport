<?php

namespace App\Twig;

use App\Entity\Prospect;

class AdminExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter(
                'filter_admin_actions',
                [$this, 'filterActions']
            )
        ];

    }

    public function filterActions(array $itemActions, $item)
    {

        if ($item instanceof  Prospect){
            unset($itemActions['export']);
        }
        return $itemActions;
    }
}