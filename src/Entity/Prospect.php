<?php

namespace App\Entity;


use Doctrine\Common\EventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="prospect")
 * @ORM\HasLifecycleCallbacks()
 */
class Prospect
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     */
    private $contact;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $street;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $postCode;

    /**
     * @ORM\Column(type="string")
     */
    private $kbo;
    /**
     * @ORM\Column(type="string")
     */
    private $email;
    /**
     * @ORM\Column(type="string")
     */
    private $telephone;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sector;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $extra;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $products;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $optionalsBa;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $optionalsCmr;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $optionalsVrd;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $extrasVrd;
    /**
     * @ORM\Column(type="string")
     */
    private $tractor;
    /**
     * @ORM\Column(type="string")
     */
    private $trailers;
    /**
     * @ORM\Column(type="boolean")
     */
    private $hasTruck;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $activity;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $otherActivity;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $goods;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isRisky;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $otherRisks;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasRegulation;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $regulations;
    /**
     * @ORM\Column(type="string")
     */
    private $region;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $extraRegion;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasBranch;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $extraBranch;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $isContracter;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $contracterCountries;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasAdditional;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $additionalDetails;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $turnover;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $employees;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $senderEmail;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lang;

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @ORM\PreFlush()
     */
    public function setLang()
    {
        $this->lang = $GLOBALS['request']->getLocale();
    }
    /**
     * @ORM\Column(type="boolean")
     */
    private $isSentEmail = false;

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param mixed $postCode
     * @return Prospect
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getOtherActivity()
    {
        return $this->otherActivity;
    }

    /**
     * @param mixed $otherActivity
     */
    public function setOtherActivity($otherActivity)
    {
        $this->otherActivity = $otherActivity;
    }

    /**
     * @return mixed
     */
    public function getOtherRisks()
    {
        return $this->otherRisks;
    }

    /**
     * @param mixed $otherRisks
     */
    public function setOtherRisks($otherRisks)
    {
        $this->otherRisks = $otherRisks;
    }

    /**
     * @return mixed
     */
    public function getExtraBranch()
    {
        return $this->extraBranch;
    }

    /**
     * @param mixed $extraBranch
     */
    public function setExtraBranch($extraBranch)
    {
        $this->extraBranch = $extraBranch;
    }

    /**
     * @return mixed
     */
    public function getOptionalsBa()
    {
        return $this->optionalsBa;
    }

    /**
     * @param mixed $optionalsBa
     */
    public function setOptionalsBa($optionalsBa)
    {
        $this->optionalsBa = $optionalsBa;
    }

    /**
     * @return mixed
     */
    public function getOptionalsCmr()
    {
        return $this->optionalsCmr;
    }

    /**
     * @param mixed $optionalsCmr
     */
    public function setOptionalsCmr($optionalsCmr)
    {
        $this->optionalsCmr = $optionalsCmr;
    }

    /**
     * @return mixed
     */
    public function getOptionalsVrd()
    {
        return $this->optionalsVrd;
    }

    /**
     * @param mixed $optionalsVrd
     */
    public function setOptionalsVrd($optionalsVrd)
    {
        $this->optionalsVrd = $optionalsVrd;
    }

    /**
     * @return mixed
     */
    public function getExtrasVrd()
    {
        return $this->extrasVrd;
    }

    /**
     * @param mixed $extrasVrd
     */
    public function setExtrasVrd($extrasVrd)
    {
        $this->extrasVrd = $extrasVrd;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Prospect
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     * @return Prospect
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     * @return Prospect
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTractor()
    {
        return $this->tractor;
    }

    /**
     * @param mixed $tractor
     * @return Prospect
     */
    public function setTractor($tractor)
    {
        $this->tractor = $tractor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrailers()
    {
        return $this->trailers;
    }

    /**
     * @param mixed $trailers
     * @return Prospect
     */
    public function setTrailers($trailers)
    {
        $this->trailers = $trailers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasTruck()
    {
        return $this->hasTruck;
    }

    /**
     * @param mixed $hasTruck
     * @return Prospect
     */
    public function setHasTruck($hasTruck)
    {
        $this->hasTruck = $hasTruck;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * @param mixed $activity
     * @return Prospect
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * @param mixed $goods
     * @return Prospect
     */
    public function setGoods($goods)
    {
        $this->goods = $goods;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisRisky()
    {
        return $this->isRisky;
    }

    /**
     * @param mixed $isRisky
     * @return Prospect
     */
    public function setIsRisky($isRisky)
    {
        $this->isRisky = $isRisky;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasRegulation()
    {
        return $this->hasRegulation;
    }

    /**
     * @param mixed $hasRegulation
     * @return Prospect
     */
    public function setHasRegulation($hasRegulation)
    {
        $this->hasRegulation = $hasRegulation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegulations()
    {
        return $this->regulations;
    }

    /**
     * @param mixed $regulations
     * @return Prospect
     */
    public function setRegulations($regulations)
    {
        $this->regulations = $regulations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     * @return Prospect
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtraRegion()
    {
        return $this->extraRegion;
    }

    /**
     * @param mixed $extraRegion
     * @return Prospect
     */
    public function setExtraRegion($extraRegion)
    {
        $this->extraRegion = $extraRegion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasBranch()
    {
        return $this->hasBranch;
    }

    /**
     * @param mixed $hasBranch
     * @return Prospect
     */
    public function setHasBranch($hasBranch)
    {
        $this->hasBranch = $hasBranch;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisContracter()
    {
        return $this->isContracter;
    }

    /**
     * @param mixed $isContracter
     * @return Prospect
     */
    public function setIsContracter($isContracter)
    {
        $this->isContracter = $isContracter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContracterCountries()
    {
        return $this->contracterCountries;
    }

    /**
     * @param mixed $contracterCountries
     * @return Prospect
     */
    public function setContracterCountries($contracterCountries)
    {
        $this->contracterCountries = $contracterCountries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasAdditional()
    {
        return $this->hasAdditional;
    }

    /**
     * @param mixed $hasAdditional
     * @return Prospect
     */
    public function setHasAdditional($hasAdditional)
    {
        $this->hasAdditional = $hasAdditional;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdditionalDetails()
    {
        return $this->additionalDetails;
    }

    /**
     * @param mixed $additionalDetails
     * @return Prospect
     */
    public function setAdditionalDetails($additionalDetails)
    {
        $this->additionalDetails = $additionalDetails;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * @param mixed $turnover
     * @return Prospect
     */
    public function setTurnover($turnover)
    {
        $this->turnover = $turnover;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param mixed $employees
     * @return Prospect
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Prospect
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Prospect
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     * @return Prospect
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * @param mixed $senderEmail
     * @return Prospect
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisSentEmail()
    {
        return $this->isSentEmail;
    }

    /**
     * @param mixed $isSentEmail
     * @return Prospect
     */
    public function setIsSentEmail($isSentEmail)
    {
        $this->isSentEmail = $isSentEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKbo()
    {
        return $this->kbo;
    }

    /**
     * @param mixed $kbo
     * @return Prospect
     */
    public function setKbo($kbo)
    {
        $this->kbo = $kbo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     * @return Prospect
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * @param mixed $sector
     * @return Prospect
     */
    public function setSector($sector)
    {
        $this->sector = $sector;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param mixed $extra
     * @return Prospect
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;
        return $this;
    }




}





 
