<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $name;

    /**
     * @ORM\Column(type="text")
     */
    public $descriptionNl;
    /**
     * @ORM\Column(type="text",nullable=true)
     */
    public $descriptionFr;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $optionals;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $termsNl;
    /**
     * @Vich\UploadableField(mapping="product_terms", fileNameProperty="termsNl")
     * @var File
     */
    private $termsFileNl;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $termsFr;
    /**
     * @Vich\UploadableField(mapping="product_terms", fileNameProperty="termsFr")
     * @var File
     */
    private $termsFileFr;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;
    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updated;

    /**
     * @return mixed
     */
    public function getDescriptionFr()
    {
        return $this->descriptionFr;
    }

    /**
     * @param mixed $descriptionFr
     */
    public function setDescriptionFr($descriptionFr)
    {
        $this->descriptionFr = $descriptionFr;
    }

    /**
     * @return mixed
     */
    public function getOptionals()
    {
        return $this->optionals;
    }

    /**
     * @param mixed $optionals
     */
    public function setOptionals($optionals)
    {
        $this->optionals = $optionals;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescriptionNl()
    {
        return $this->descriptionNl;
    }

    /**
     * @param mixed $descriptionNl
     * @return Product
     */
    public function setDescriptionNl($descriptionNl)
    {
        $this->descriptionNl = $descriptionNl;
        return $this;
    }

    /**
     * @return string
     */
    public function getTermsNl()
    {
        return $this->termsNl;
    }

    /**
     * @param string $termsNl
     * @return Product
     */
    public function setTermsNl(string $termsNl)
    {
        $this->termsNl = $termsNl;
        return $this;
    }

    /**
     * @return File
     */
    public function getTermsFileNl()
    {
        return $this->termsFileNl;
    }

    /**
     * @param File $termsFileNl
     * @return Product
     */
    public function setTermsFileNl(File $termsFileNl)
    {
        $this->termsFileNl = $termsFileNl;
        return $this;
    }

    /**
     * @return string
     */
    public function getTermsFr()
    {
        return $this->termsFr;
    }

    /**
     * @param string $termsFr
     * @return Product
     */
    public function setTermsFr(string $termsFr)
    {
        $this->termsFr = $termsFr;
        return $this;
    }

    /**
     * @return File
     */
    public function getTermsFileFr()
    {
        return $this->termsFileFr;
    }

    /**
     * @param File $termsFileFr
     * @return Product
     */
    public function setTermsFileFr(File $termsFileFr)
    {
        $this->termsFileFr = $termsFileFr;
        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageFile($image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updated = new \DateTime('now');
        }
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated = new \DateTime("now");
    }

}