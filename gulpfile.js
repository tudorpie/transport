var gulp = require('gulp'),
sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer'),
hash = require('gulp-hash'),
del = require('del'),
sourcemaps = require('gulp-sourcemaps'),
browserSync = require('browser-sync').create()


// Static server
// -------------
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: {
            baseDir: "public/"
        }
    });
    gulp.watch('public/assets/scss/**/*.scss', ['sass']);
    gulp.watch("public/assets/**/*.html").on('change', browserSync.reload);
});

// Watch and process SCSS files
// ------------------
gulp.task('sass', function () {
  //Delete our old css files
  del(["public/assets/css/main.css"])
  //compile hashed css files
  return gulp.src('public/assets/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({  // Passes it through autoprefixer
      browsers: ['last 2 versions'],
      cascade: false
        }))
    //.pipe(hash())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('public/assets/css'))
    //.pipe(hash.manifest("hash.json")) //Create a hash map
    //.pipe(gulp.dest("app/css")) //Put the map in the data directory
    .pipe(browserSync.stream());
});

// Cleaning
gulp.task('clean', function(callback) {
  del('public');
  return cache.clearAll(callback);
});

// Build Sequences
// ---------------
gulp.task("default", ["serve"])
