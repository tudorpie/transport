$(document).ready(function() {
  var emailFilledIn = $(".email").val();
  $("input.required").blur(function() {
    if ($(this).prop("id") != "day") {
      if ($.trim($(this).val()).length === 0) {
        $(this).removeClass("valid").addClass("error");
      } else {
        $(this).removeClass("error").addClass("valid");
        $(this).parents(".form__input").next(".form__error").remove();
        if ($(this).hasClass('email')) {
          if ($(this).hasClass('required') && $(this).hasClass('email') && $(this).val() !== '') {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test($(this).val())) {
              $(this).removeClass('valid').addClass('error');
              isFormValid = false;
            }
          }
        }
      }
    }
  });
  $("select.required").on('change', function() {
    if ($(this).find("option:selected").val() === '') {
      $(this).parents(".dropdown").removeClass('valid').addClass('error');
      isFormValid = false;
    }
    else{
      $(this).parents(".dropdown").removeClass('error').addClass('valid');
      $(this).parents(".form__input").next(".form__error").remove();
    }
  });
  $(".checkboxes input[type=checkbox]").on('change', function() {
    if ($(this).is(':checked')) {
      $(this).parents(".dropdown").removeClass('error').addClass('valid');
      $(this).parents(".form__input").next(".form__error").remove();
    }
  });
});
$('#submitButton').on("click", function(e) {
  e.preventDefault();
  var isFormValid = true;
  $('.form__error').remove();
  // CHECK IF SELECT HAS A VALID OPTION SELECTED
  $(this).parents("body").find('form select.required').each(function() {
    if ($(this).find("option:selected").val() === '') {
      $(this).parents(".dropdown").removeClass('valid').addClass('error');
      $(this).parents('.form__input').after('<div class="form__error"><span class="error-msg__content">' + $(this).data('error') + '</span></div>');
      isFormValid = false;
    }
  });
  // CHECK IF AT LEAST ONE CHECKBOX IS CHECKED
  $(this).parents("body").find('form .checkboxes.required').each(function() {
    if (!$(this).find("input[type=checkbox]:checked").length) {
      $(this).parents('.form__input').after('<div class="form__error"><span class="error-msg__content">' + $(this).data('error') + '</span></div>');
      isFormValid = false;
    }
    else{
      $(this).parents(".form__input").next(".form__error").remove();
    }
  });

  $(this).parents("body").find('form input.required').each(function() {
    if ($.trim($(this).val()).length === 0) {
      $(this).removeClass("valid").addClass("error");
      $(this).parents(".form__input").after('<div class="form__error"><span class="error-msg__content">' + $(this).data('error') + '</span></div>');
      isFormValid = false;
    } else {
      $(this).removeClass("error").addClass("valid");
      $(this).parents(".form__input").next(".form__error").remove();
      if ($(this).hasClass('email')) {
        if ($(this).hasClass('required') && $(this).hasClass('email') && $(this).val() !== '') {
          var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
          if (!pattern.test($(this).val())) {
            $(this).removeClass('valid').addClass('error');
            isFormValid = false;
          }
        }
      }
    }
  });
  if (isFormValid) {
    //$(this).parents("form").submit();
    console.log("valid form!");
    //$("#submitButton").attr('disabled','disabled');

  }
  return isFormValid;
});
