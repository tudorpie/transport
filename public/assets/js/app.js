// GET URL PARAMETER
var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

		for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
						return sParameterName[1] === 'undefined' ? true : sParameterName[1];
				}
		}
};

// SMOOTH SCROLL
	$('a[href*=\\#]:not([href=\\#])').on('click', function() {
	if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		debugger;
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		var submenu = $('.drop-shadow').height();
		if (target.length) {
			$('html,body').animate({
				scrollTop: target.offset().top - submenu - 18
			}, 1000);
			return false;
		}
	}
});

(function(){
	// FIX ON SCROLL
	if($('.submenu').length){
		var fixmeTop = $('.submenu').offset().top;
		$(window).scroll(function() {                  // assign scroll event listener

		    var currentScroll = $(window).scrollTop(); // get current position

		    if (currentScroll >= fixmeTop) {           // apply position: fixed if you
		        $('.submenu').addClass("submenu--fixed");
		    } else {                                   // apply position: static
		        $('.submenu').removeClass("submenu--fixed");
		    }

		});
	}
	$( "#inp_birthDate" ).datepicker({
      firstDay: 1,
      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+0",
			maxDate: '-6Y',
      onClose: function(){
        if($(this).val() !== ''){
          $(this).removeClass('error');
        }
      }
    });
	$(".only-letters").on('keyup', function(){
		$(this).val($(this).val().replace(/[^a-zA-Z-]/g, ''));
	});
	$(".only-numbers").on('keyup', function(){
		$(this).val($(this).val().replace(/[^0-9]/g, ''));
	});

	// MODAL CLOSE
	$(".md__close").on('click', function(e){
		e.preventDefault();
		$(".md, .md-overlay").addClass('is-hidden');
		$("body").removeClass("md-open");
	});
	// CALC MODAL
	$("#showCalc").on('click', function(e){
		e.preventDefault();
		$(".md, .md-overlay").removeClass('is-hidden');
		$("body").addClass("md-open");
	});
	$("#mdCalcValues").on('change', 'input', function(){
		var total = 0;
		$("#mdCalcValues input").each(function(){
			total += parseInt($(this).val(), 10);
		});
		$("#mdCalcTotal").val(total);
	});
	$("#mdGetTotal").on('click', function(e){
		e.preventDefault();
		if($("#mdCalcTotal").val() >= 3 && $("#mdCalcTotal").val() <= 13){
			$("#errRooms").addClass('is-hidden');
			$("#houseRooms").val($("#mdCalcTotal").val());
			$(".md, .md-overlay").addClass('is-hidden');
		}
		else{
			$("#errRooms").removeClass('is-hidden');
		}
	});

	//SECURITY-CHECKBOX
	$(".security input[type=checkbox]").on('change', function(){
		if($(this).is(':checked')){
			$(this).parents('.security-row').addClass('security-row--checked');
		}
		else{
			$(this).parents('.security-row').removeClass('security-row--checked');
		}
	});
	//BROKER SELECT
	$(".brokers input[type=radio]").on('change', function(){
		if($(this).is(':checked')){
			$(this).parents('.brokers').find('.broker').removeClass('broker--checked');
			$(this).parents('.broker').addClass('broker--checked');
		}
		else{
			$(this).parents('.broker').removeClass('broker--checked');
		}
	});


	// CHECKS BEFORE SHOWING CERTAIN FIELDS
	$("input[name=typegebouw]").on('change', function(){
			checkWantInsurance();
			checkNaturalDisaster();
	});
	$("input[name=youare]").on('change', function(){
			checkWantInsurance();
	});
	$("input[name=theft]").on('change', function(){
			checkWantTheftInsurance();
	});

	// ALSO CHECK ON DOCUMENT READY
	checkWantTheftInsurance();

	// FUNCTIONS FOR CHECKS
	function checkWantInsurance(){
		if($("input[name=typegebouw]:checked").val() == 'Appartement' && $("input[name=youare]:checked").val() == 'Eigenaar - Bewoner'){
			$("#hdn_buildingInsurance").removeClass('is-hidden');
		}
		else{
			$("#hdn_buildingInsurance").addClass('is-hidden');
		}
	}
	function checkNaturalDisaster(){
		if($("input[name=typegebouw]:checked").val() == 'Appartement' && $("input[name=NaturalDisasterTariffClassCode]").val() == '6'){
			$("#hdn_appGroundFloor, #hdn_basement, #hdn_storageGroundFloor").removeClass('is-hidden');
		}
		else{
			$("#hdn_appGroundFloor, #hdn_basement, #hdn_storageGroundFloor").addClass('is-hidden');
		}
	}
	function checkWantTheftInsurance(){
		if($("input[name=theft]:checked").val() == 'Ja'){
			$("#hdn_detachedHouse, #hdn_theftAlarm").removeClass('is-hidden');
			$("#rad_simulation_3").parent('li').removeClass('is-hidden');
		}
		else{
			$("#hdn_detachedHouse, #hdn_theftAlarm").addClass('is-hidden');
			$("#rad_simulation_3").parent('li').addClass('is-hidden');
		}
	}

	//=====================================
	// CALCULATIONS
	//=====================================

	// CREATE TOTAL ON STEP 2
		// On change, calculate total
	$("input[name=simulations], .security input").on('change', function(){
		$("#security_totalprice").html(getTotalStep2());
	});
		// Function to calculate total
	function getTotalStep2(){
		var totalPrice = 0;
		var simulationPrice = 0;
		var totalOtherInsurances = 0;
		// Get simulation price
		simulationPrice = parseFloat($("input[name=simulations]:checked").parents('.simulation').find(".simulation__totalprice").html().replace(/,/g, '.'));
		// Get other checked insurances
		$('.security input:checked').each(function () {
			totalOtherInsurances += parseFloat($(this).val().replace(/,/g, '.'));
		});
		// Calculate total
		totalPrice = parseFloat(simulationPrice + totalOtherInsurances);
		$("#toStep3").attr("href", "step-3.html?priceStep2=" + totalPrice.toFixed(2));
		// Return total
		return totalPrice.toFixed(2);
	}
	// GET PRICE ON STEP 3
	$("#simulationTotal").html(getUrlParameter('priceStep2'));



	//=====================================
	// SWITCH BUTTONS STEP 3
	//=====================================
		$("#map__broker").on('click', function(){
			$('.map__header button').removeClass('active');
			$(".map__search__form").addClass('is-hidden');
			$("#map__search__broker, .map__search").removeClass('is-hidden');
			$(this).addClass('active');
		});
		$("#map__postalcode").on('click', function(){
			$('.map__header button').removeClass('active');
			$(".map__search__form").addClass('is-hidden');
			$("#map__search__postalcode, .map__search").removeClass('is-hidden');
			$(this).addClass('active');
		});
		// for location button, see map.js

})();
