$( document ).ready(function() {
  $("#addEmail").on("click", function(e){
    e.preventDefault();
    addEmail();
  });
  $("#rapportEmails").on("click", ".rapport__email button", function(e){
    e.preventDefault();
    removeEmail($(this).parents(".rapport__email"));

  });
});
function addEmail(){
  var email = $("#rapportEmail").val();
  var emailList = $('#emailList').val();
  if(checkEmail(email)){
    $("#rapportEmails").append('<li class="rapport__email"><span class="email">' + email + '</span><button type="button"><span class="fa fa-close"></span></button></li>');
      $('#rapportEmail').val('');
      $('#emailList').val(email + ';' + emailList);
  }
};

function removeEmail(email){
    var emailList =     $('#emailList').val().replace(email.text()+';','');
    $('#emailList').val(emailList);
    email.remove();
}

function checkEmail(email){
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  if (pattern.test(email)) {
    return true;
  }
  else{
    return false;
  }
};
